#! /bin/bash

echo "[...] Checking internet connection [... ]" #Affiche le texte qui suit
ping -c4 8.8.8.8 >> /dev/null 		#Ping le serveur DNS de Google
PING=$?		#Créer une variable lié au ping	
 if [ $PING -ne 0 ] 	#condition si le ping n'est pas égale à 0
	then    {
		 echo "[/!\] Not connected to Internet    [/!\]     #alors affiche ...
		       [/!\] Please check configuration   [/!\]"    #alors affiche ...
		}
	else    {
		 echo "[...] Internet access OK [...]"		#alors affiche ...
		}
fi		#fin de la boucle		
